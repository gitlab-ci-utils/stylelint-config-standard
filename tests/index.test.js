import { describe, expect, it } from 'vitest';
import config from '../index.js';
import stylelint from 'stylelint';

const fileTestAllPass = './tests/fixtures/test-all-pass.css';
const fileTestSomeFail = './tests/fixtures/test-some-fail.css';

describe('test config', () => {
    it('should validate CSS default config and find no parsing errors', async () => {
        expect.assertions(1);
        const result = await stylelint.lint({ config, files: fileTestAllPass });
        const report = JSON.parse(result.report);
        expect(report[0].parseErrors).toHaveLength(0);
    });

    it('should validate CSS with no errors and find no errors', async () => {
        expect.assertions(1);
        const result = await stylelint.lint({ config, files: fileTestAllPass });
        const report = JSON.parse(result.report);
        expect(report[0].errored).toBe(false);
    });

    it('should validate CSS with errors and find correct errors', async () => {
        expect.assertions(2);
        const result = await stylelint.lint({
            config,
            files: fileTestSomeFail
        });
        const report = JSON.parse(result.report);
        expect(report[0].errored).toBe(true);
        expect(report[0].warnings).toMatchSnapshot();
    });
});
