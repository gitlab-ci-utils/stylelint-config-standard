# Stylelint Config Standard

Custom standard Stylelint configuration for all projects. Includes all
referenced configurations and plugins as dependencies so they don't need
to be included separately.

To setup `.stylelintrc.json` with this configuration alone:

```json
{
  "extends": ["@aarongoldenthal/stylelint-config-standard"]
}
```
